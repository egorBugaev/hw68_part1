import axios from '../axios-counter';

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';
export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_ADD = 'FETCH_COUNTER_ADD';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';


export const fetchCounterRequest = () => {
    return { type: FETCH_COUNTER_REQUEST };
};


export const fetchCounterSuccess = (counter) => {
    return { type: FETCH_COUNTER_SUCCESS, counter};
};

export const fetchCounterError = () => {
    return { type: FETCH_COUNTER_ERROR };
};


export const incrementCounter = () => {
    return dispatch => {
        dispatch({ type: INCREMENT });
        dispatch(addCounterVal())
    };
};

export const decrementCounter = () => {
    return dispatch => {
        dispatch({ type: DECREMENT });
        dispatch(addCounterVal())
    };
};

export const addCounter = (amount) => {
    return dispatch => {
        dispatch({ type: ADD, amount});
        dispatch(addCounterVal())
    };

};

export const subtractCounter = (amount) => {
    return dispatch => {
        dispatch({ type: SUBTRACT, amount});
        dispatch(addCounterVal())
    };
};

export const fetchCounter = () => {
    return dispatch => {
        dispatch(fetchCounterRequest());
        axios.get('/counter.json').then(response => {
            dispatch(fetchCounterSuccess(response.data));
        }, error => {
            dispatch(fetchCounterError());
        });
    }
};
export const addCounterVal = () => {
    return (dispatch, getState) => {
        const counter = getState().counter;
        axios.put('/counter.json', counter);
    }
};
